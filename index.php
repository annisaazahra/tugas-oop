<?php
require_once ("Animal.php");
require_once ("Ape.php");
require_once ("Frog.php");

$sheep = new Animal("Shaun");

echo "The First Animal <br>";
echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "Legs : " . $sheep->legs . "<br>"; // 4
echo "Cold Blooded : " . $sheep->cold_blooded . "<br><br>"; // "no"

$sungokong = new Ape("Kera Sakti");
echo "The Second Animal <br>";
echo "Name : " . $sungokong-> name . "<br>";
echo "Legs : " . $sungokong-> legs . "<br>";
echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>";
echo "Yell : " . $sungokong->yell() . "<br><br>";

$kodok = new Frog("Buduk");
echo "The Third Animal <br>";
echo "Name : " . $kodok-> name . "<br>";
echo "Legs : " . $kodok-> legs . "<br>";
echo "Cold Blooded : " . $kodok->cold_blooded . "<br>";
echo "Jump : " . $kodok->jump() . "<br><br>";

?>